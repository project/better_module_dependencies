--------------------------------------------------------------------------------

                        BETTER MODULE DEPENDENCIES

--------------------------------------------------------------------------------

CONTENTS OF THIS FILE:
---------------------

- Introduction
- Installation
- User Interface
- Un-installation


--------------------------------------------------------------------------------

INTRODUCTION:
------------

This module enhances modules list page and comes in handy if you have a large
amount of modules installed on your Drupal website. 

In cases like that each module's requirements list becomes larger and larger,
therefore it becomes very hard to read and work with module dependencies.

So what this module actually does is making any module dependency clickable.
1) Clicking with your mouse on any of the required modules makes your screen
   scroll to the module that you've clicked on.
2) If the required module is missing, clicking on it will open a new tab with 
   the module's page on drupal.org

These small features are very helpful when you are trying to find your module as
 fast as possible.


--------------------------------------------------------------------------------

INSTALLATION:
------------

To install this module
1) Go to admin/modules
2) Find 'Better Module Dependencies'
3) Check the box on left and save.


--------------------------------------------------------------------------------

UNINSTALLATION:
---------------

To uninstall the module,
1) Go to admin/modules
2) Find 'Better Module Dependencies'
3) Un-check the checkbox and save.
4) Then go to admin/modules/uninstall
5) check the checkbox next to 'Better Module Dependencies' and click 'uninstall'.


--------------------------------------------------------------------------------

                                Thank You!
